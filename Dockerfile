FROM registry.gitlab.com/max-develop/wildfy-pq

ARG WAR_FILE=jsf-todo.war

COPY target/${WAR_FILE} /opt/jboss/wildfly/standalone/deployments

# Lables
LABEL de.openknowledge.image.author="Max Weis"
LABEL de.openknowledge.image.email="max.weis@openknowledge.de"
LABEL de.openknowledge.image.title="jsf-todo"
LABEL de.openknowledge.image.description="A example JSF App running inside a Wildfly container connected to a Postgres Database"
LABEL de.openknowledge.image.vcs-url="https://gitlab.com/max-openknowledge/jsf-demo"
