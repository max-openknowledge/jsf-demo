package domain;

import com.github.kaiwinter.instantiator.InjectionObjectFactory;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.PersistenceContext;
import java.util.Date;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;

@Testcontainers
public class TodoRepositoryIT {

  private static final Logger LOG = LoggerFactory.getLogger(TodoRepositoryIT.class);

  private static EntityManager entityManager = Persistence.createEntityManagerFactory("TestPU").createEntityManager();

  private static TodoRepository repository;

  static {
    InjectionObjectFactory factory = new InjectionObjectFactory(PersistenceContext.class);
    factory.setImplementationForClassOrInterface(EntityManager.class, entityManager);
    repository = new TodoRepository(entityManager);
  }

  @Container
  private static PostgreSQLContainer postgreSQLContainer = new PostgreSQLContainer()
      .withDatabaseName("postgres")
      .withUsername("postgres")
      .withPassword("postgres");

  @Before
  public void setup() {
    // Rolling back transaction will make tests after a failed test run correctly
    if (entityManager.getTransaction().isActive()) {
      entityManager.getTransaction().rollback();
    }
    // Clear hibernate cache, else inserted testdata may cause trouble
    entityManager.clear();
  }

  @Test
  public void testRepository() {
    assertThat(repository).isNotNull();
  }

  @Test
  public void testContainersIsAvailable() {
    LOG.info(postgreSQLContainer.getJdbcUrl());
    assertThat(postgreSQLContainer.isRunning()).isTrue();
  }

  @Test
  public void testFind() {
    Todo todo = repository.find(2);
    assertThat(todo.getName()).isEqualTo("title2");
    assertThat(todo.getDescription()).isEqualTo("clean fridge2");
    assertThat(todo.getDueDate().toString()).isEqualTo("2015-12-01 00:00:00.0");
    assertThat(todo.isState()).isEqualTo(true);
  }

  @Test
  public void findShouldFailForWrongId() {
    assertThatThrownBy(() -> repository.find(99)).isInstanceOf(IllegalArgumentException.class);
  }

  @Test
  public void testFindAll() {
    assertThat(this.repository.findAll().size()).isEqualTo(12);
  }

  @Test
  public void testUpdate() {
    Todo todo = this.repository.find(1);
    todo.setState(true);

    this.repository.update(todo);

    assertThat(this.repository.find(1)).isEqualTo(todo);
  }

  @Test
  public void testSave() {
    Todo todo = Todo.newBuilder().withName("new").withDescription("new").withDueDate(new Date()).withState(true).build();

    Todo newTodo = this.repository.save(todo);

    assertThat(newTodo.getName()).isEqualTo(todo.getName());
    assertThat(newTodo.getDescription()).isEqualTo(todo.getDescription());
    assertThat(newTodo.getDueDate()).isEqualTo(todo.getDueDate());
    assertThat(newTodo.isState()).isEqualTo(todo.isState());
  }

  @Test
  public void testDelete() {
    Todo todo = this.repository.find(12);
    Todo deletedTodo = this.repository.delete(todo.getId());

    assertThat(deletedTodo.getName()).isEqualTo(todo.getName());
    assertThat(deletedTodo.getDescription()).isEqualTo(todo.getDescription());
    assertThat(deletedTodo.getDueDate()).isEqualTo(todo.getDueDate());
    assertThat(deletedTodo.isState()).isEqualTo(todo.isState());
  }

  @Test
  public void deleteShouldFailForWrongId() {
    assertThatThrownBy(() -> this.repository.delete(99)).isInstanceOf(IllegalArgumentException.class);
  }
}
