package application;

import domain.Todo;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@Testcontainers
public class SeleniumIT {

  private String SERVICE_URL;

  private RemoteWebDriver driver;

  private SeleniumTestContainer seleniumTestContainers;

  @BeforeAll
  public void setUp() {
    seleniumTestContainers = new SeleniumTestContainer();
    seleniumTestContainers.start();

    SERVICE_URL = seleniumTestContainers.getURL();
  }

  @Test
  public void testTitle() {
    driver = seleniumTestContainers.getDriver();
    driver.get(SERVICE_URL);
    assertThat(driver.getTitle()).isEqualTo("Todo Demo | OPEN KNOWLEDGE GmbH");
  }

  @Test
  public void testRow() {
    driver = seleniumTestContainers.getDriver();
    driver.get(SERVICE_URL);

    String Title = driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[2]/td[2]/span"))
        .getText();
    String Description = driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[2]/td[3]/span"))
        .getText();
    String Due = driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[2]/td[4]/span"))
        .getText();

    String expectedTitle = "title2";
    String expectedDescription = "clean fridge2";
    String expectedDue = "01.12.2015";

    assertThat(expectedTitle).isEqualTo(Title);
    assertThat(expectedDescription).isEqualTo(Description);
    assertThat(expectedDue).isEqualTo(Due);
  }

  @Test
  void testDeleteTodo() {
    driver = seleniumTestContainers.getDriver();
    driver.get(SERVICE_URL);

    driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[2]/td[5]/input[1]"))
        .click();

    int rowCount = driver
        .findElements(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr"))
        .size();

    assertThat(11).isEqualTo(rowCount);
  }

  //@Test
  public void testAddNewTodo() {
    Todo todo = Todo
        .newBuilder()
        .withName("New Todo")
        .withDescription("New Todo")
        .withDueDate(new Date())
        .withState(false)
        .build();

    driver = seleniumTestContainers.getDriver();
    driver.get(SERVICE_URL);

    driver
        .findElement(By.id("j_idt37:name"))
        .sendKeys(todo.getName());

    driver
        .findElement(By.id("j_idt37:description"))
        .sendKeys(todo.getDescription());

    driver
        .findElement(By.id("j_idt37:dueDate_input"))
        .sendKeys(todo
            .getDueDate()
            .toString());

    driver
        .findElement(By.id("j_idt37:j_idt46"))
        .click();

    List<WebElement> elements = driver.findElements(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr"));

    WebElement lastRow = elements.get(elements.size() - 1);

    List<WebElement> spans = lastRow.findElements(By.name("span"));

    assertThat(todo.getName()).isEqualTo(spans
        .get(1)
        .getText());
    assertThat(todo.getDescription()).isEqualTo(spans
        .get(2)
        .getText());
    assertThat(todo.getDueDate()).isEqualTo(spans
        .get(3)
        .getText());
  }

  @Test
  public void testUpdateFirst() {
    driver = seleniumTestContainers.getDriver();
    driver.get(SERVICE_URL);

    String newTodo = "New Todo";
    String newDescription = "New Todo";
    String newDueDateFormat = "09/01/20";
    String newDueDate = "01.09.2020";

    // click edit button
    driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[1]/td[5]/input[2]"))
        .click();
    // clear input
    driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[1]/td[2]/input"))
        .clear();
    // add input
    driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[1]/td[2]/input"))
        .sendKeys(newTodo);
    // clear input
    driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[1]/td[3]/input"))
        .clear();
    // add input
    driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[1]/td[3]/input"))
        .sendKeys(newDescription);
    // clear input
    driver
        .findElement(By.id("todoTable:j_idt16:0:j_idt31_input"))
        .clear();
    driver
        .findElement(By.id("todoTable:j_idt16:0:j_idt31_input"))
        .sendKeys(newDueDateFormat);

    // click save button
    driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[1]/td[5]/input[2]"))
        .click();

    String title = driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[1]/td[2]/span"))
        .getText();
    String description = driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[1]/td[3]/span"))
        .getText();
    String due = driver
        .findElement(By.xpath("/html/body/div[1]/div/div[1]/form/table/tbody/tr[1]/td[4]/span"))
        .getText();

    assertThat(newTodo).isEqualTo(title);
    assertThat(newDescription).isEqualTo(description);
    assertThat(newDueDate).isEqualTo(due);
  }

  @AfterAll
  public void tearDown() {
    seleniumTestContainers.stop();
  }
}
