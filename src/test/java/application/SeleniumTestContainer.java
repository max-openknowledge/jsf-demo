package application;

import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testcontainers.containers.BrowserWebDriverContainer;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.Network;
import org.testcontainers.containers.PostgreSQLContainer;

public class SeleniumTestContainer {

  private GenericContainer jsfServiceContainer;

  private PostgreSQLContainer postgreSQLContainer;

  private BrowserWebDriverContainer chromeContainer;

  private Network network;

  private String URL;

  public SeleniumTestContainer() {
    network = Network.newNetwork();

    postgreSQLContainer = (PostgreSQLContainer)new PostgreSQLContainer()
        .withDatabaseName("postgres")
        .withUsername("postgres")
        .withPassword("postgres")
        .withNetwork(network)
        .withNetworkAliases("postgresql");

    jsfServiceContainer = new GenericContainer("baroprime/jsf-todo")
        .withExposedPorts(8080)
        .withNetwork(network)
        .withNetworkAliases("jsf-service");

    chromeContainer = (BrowserWebDriverContainer)new BrowserWebDriverContainer()
        .withCapabilities(new ChromeOptions())
        .withNetwork(network);

    URL = "http://jsf-service:8080/jsf-todo/index.jsf";
  }

  public String getURL() {
    return this.URL;
  }

  public RemoteWebDriver getDriver() {
    return this.chromeContainer.getWebDriver();
  }

  public void start() {
    postgreSQLContainer.start();
    jsfServiceContainer.start();
    chromeContainer.start();
  }

  public void stop() {
    postgreSQLContainer.stop();
    jsfServiceContainer.stop();
    chromeContainer.stop();
  }
}
