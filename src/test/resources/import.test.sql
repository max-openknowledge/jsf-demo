
CREATE TABLE IF NOT EXISTS tab_todo (
    "pk_todo_id" integer,
    "todo_name" text,
    "todo_description" text,
    "todo_due_date" date,
    "todo_state" boolean,
    PRIMARY KEY( pk_todo_id )
    );
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (1, 'title1','clean fridge1', '2015-12-01', false);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (2, 'title2','clean fridge2', '2015-12-01', true);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (3, 'title3','clean fridge3', '2015-12-01', false);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (4, 'title4','clean fridge4', '2015-12-01', true);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (5, 'title5','clean fridge5', '2015-12-01', false);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (6, 'title6','clean fridge6', '2015-12-01', true);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (7, 'title7','clean fridge7', '2015-12-01', false);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (8, 'title8','clean fridge8', '2015-12-01', true);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (9, 'title9','clean fridge9', '2015-12-01', false);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (10, 'title10','clean fridge10', '2015-12-01', true);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (11, 'title11','clean fridge11', '2015-12-01', false);
insert into public.TAB_TODO(PK_TODO_ID, TODO_NAME, TODO_DESCRIPTION, TODO_DUE_DATE, TODO_STATE) values (12, 'title12','clean fridge12', '2015-12-01', true);

