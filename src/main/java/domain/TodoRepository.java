package domain;

import infrastructure.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

import static org.apache.commons.lang3.Validate.notNull;

@Repository
public class TodoRepository {

  private static final Logger LOG = LoggerFactory.getLogger(TodoRepository.class);

  @Inject
  EntityManager entityManager;

  public TodoRepository() {
  }

  public TodoRepository(EntityManager entityManager) {
    this.entityManager = entityManager;
  }

  public Todo find(Integer id) {
    LOG.info("Locating todo with id {}", id);

    Todo todo = entityManager.find(Todo.class, id);

    if (todo == null) {
      LOG.warn("Found no todo with id {}", id);
      throw new IllegalArgumentException("Found no Todo");
    }

    return todo;
  }

  public List<Todo> findAll() {
    LOG.info("Locating all todos");

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Todo> cq = cb.createQuery(Todo.class);
    Root<Todo> rootEntry = cq.from(Todo.class);
    CriteriaQuery<Todo> all = cq.select(rootEntry);
    TypedQuery<Todo> allQuery = entityManager.createQuery(all);

    return allQuery.getResultList();
  }

  public Todo save(Todo todo) {
    notNull(todo, "todo must not be null");

    LOG.info("Create Todo {}", todo.toString());

    entityManager.persist(todo);

    return todo;
  }

  public Todo update(Todo todo) {
    notNull(todo, "Todo must not be null");

    LOG.info("Update todo with id {}", todo.getId());

    entityManager.merge(todo);
    return todo;
  }

  public Todo delete(Integer id) {
    try {
      LOG.info("Delete Todo with id {}", id);
      Todo todoToDelete = entityManager.find(Todo.class, id);
      entityManager.remove(todoToDelete);

      //LOG.info("Deleted Todo {}", reference);
      return todoToDelete;
    } catch (EntityNotFoundException e) {
      LOG.warn("Found no Todo with id {}", id);
      throw new IllegalArgumentException("Found no Todo");
    }
  }
}
