package domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Date;

import static org.apache.commons.lang3.Validate.notNull;

@Entity
@Table(name = "TAB_TODO")
public class Todo {

  @Id
  @Column(name = "PK_TODO_ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Column(name = "TODO_NAME")
  private String name;

  @Column(name = "TODO_DESCRIPTION")
  private String description;

  @Column(name = "TODO_DUE_DATE")
  private Date dueDate;

  @Column(name = "TODO_STATE")
  private boolean state;

  public Todo(final String name, final String description, final Date dueDate, final boolean state) {
    this.name = name;
    this.description = description;
    this.dueDate = dueDate;
    this.state = state;
  }

  public Todo() {
  }

  public Integer getId() {
    return id;
  }

  public void setId(final Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public Date getDueDate() {
    return dueDate;
  }

  public void setDueDate(final Date date) {
    this.dueDate = date;
  }

  public boolean isState() {
    return state;
  }

  public void setState(final boolean state) {
    this.state = state;
  }

  @Override
  public String toString() {
    return "Todo{" + "id=" + id + ", name='" + name + '\'' + ", description='" + description + '\'' + ", date=" + dueDate + ", state="
        + state + '}';
  }

  public static TodoBuilder newBuilder() {
    return new TodoBuilder();
  }

  public static class TodoBuilder {
    private Todo todo = new Todo();

    public TodoBuilder withId(final Integer id) {
      this.todo.id = notNull(id, "id must not be null");
      return this;
    }

    public TodoBuilder withName(final String name) {
      this.todo.name = notNull(name, "name must not be null");
      return this;
    }

    public TodoBuilder withDescription(final String description) {
      this.todo.description = notNull(description, "name must not be null");
      return this;
    }

    public TodoBuilder withDueDate(final Date date) {
      this.todo.dueDate = notNull(date, "date must not be null");
      return this;
    }

    public TodoBuilder withState(final boolean state) {
      this.todo.state = state;
      return this;
    }

    public Todo build() {
      return this.todo;
    }
  }
}
