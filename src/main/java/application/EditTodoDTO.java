package application;

import domain.Todo;

import java.util.Date;

public class EditTodoDTO {
    private Integer id;

    private String name;

    private String description;

    private Date dueDate;

    private boolean state;

    private boolean editable;

    public EditTodoDTO() {
    }


    public EditTodoDTO(Todo todo) {
        this.id = todo.getId();
        this.name = todo.getName();
        this.description = todo.getDescription();
        this.dueDate = todo.getDueDate();
        this.state = todo.isState();
        this.editable = false;
    }


    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(final Date dueDate) {
        this.dueDate = dueDate;
    }

    public boolean isState() {
        return state;
    }

    public void setState(final boolean state) {
        this.state = state;
    }

    public boolean isEditable() {
        return editable;
    }

    public void setEditable(final boolean editable) {
        this.editable = editable;
    }

    @Override
    public String toString() {
        return "EditTodoDTO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", dueDate=" + dueDate +
                ", state=" + state +
                ", editable=" + editable +
                '}';
    }
}
