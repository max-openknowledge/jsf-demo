package application;

import javax.inject.Named;
import java.util.Date;

@Named
public class BaseTodoDTO {

  private String name;

  private String description;

  private Date dueDate;

  private boolean state;

  public BaseTodoDTO() {
  }

  public BaseTodoDTO(final String name, final String description, final Date dueDate) {
    this.name = name;
    this.description = description;
    this.dueDate = dueDate;
  }

  public BaseTodoDTO(final String name, final String description, final Date dueDate, final boolean state) {
    this.name = name;
    this.description = description;
    this.dueDate = dueDate;
    this.state = state;
  }

  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = name;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(final String description) {
    this.description = description;
  }

  public Date getDueDate() {
    return dueDate;
  }

  public void setDueDate(final Date dueDate) {
    this.dueDate = dueDate;
  }

  public boolean isState() {
    return state;
  }

  public void setState(final boolean state) {
    this.state = state;
  }

  @Override
  public String toString() {
    return "TodoDTO{" + "name='" + name + '\'' + ", description='" + description + '\'' + ", dueDate=" + dueDate + ", state=" + state + '}';
  }
}
