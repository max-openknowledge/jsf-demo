package application;

import domain.Todo;
import domain.TodoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;
import java.io.Serializable;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.commons.lang3.Validate.notNull;

@Named
@ViewScoped
public class TodoController implements Serializable {

  private static final Logger LOG = LoggerFactory.getLogger(TodoController.class);

  private List<EditTodoDTO> todos;

  private BaseTodoDTO baseTodoDTO;

  @Inject
  TodoRepository todoRepository;

  @PostConstruct
  void init() {
    this.baseTodoDTO = new BaseTodoDTO();
    loadTodo();
    loadTodos();
  }

  public TodoController() {
  }

  /**
   * Returns all todos from the db and maps to editTodoDTO objects
   *
   * @return list of all todos
   */
  public List<EditTodoDTO> getAllTodos() {
    return this.todoRepository.findAll()
        .stream()
        .map(EditTodoDTO::new)
        .collect(Collectors.toList());
  }

  /**
   * Saves the values from the form to the database
   * <p>
   * reloads the data table
   */
  @Transactional
  public void add() {
    notNull(baseTodoDTO, "TodoDTO should not be null");

    Todo todo = Todo.newBuilder()
        .withName(this.baseTodoDTO.getName())
        .withDescription(this.baseTodoDTO.getDescription())
        .withDueDate(this.baseTodoDTO.getDueDate())
        .build();

    this.todoRepository.save(todo);
    loadTodos();
    loadTodo();
  }

  /**
   * Deletes a todo with the given id
   *
   * @param id which to delete
   */
  @Transactional
  public void delete(final Integer id) {
    notNull(id, "ID cannot be null");
    this.todoRepository.delete(id);
    loadTodos();
  }

  /**
   * Enables the view to edit the given todo
   *
   * @param editTodoDTO to edit
   */
  public void edit(EditTodoDTO editTodoDTO) {
    LOG.debug("Edit todo with id: {}", editTodoDTO.getId());
    editTodoDTO.setEditable(true);
  }

  /**
   * Saves the changes from the row
   *
   * @param editTodoDTO todo to save
   */
  public void saveAction(EditTodoDTO editTodoDTO) {
    LOG.debug("Save todo with id: {}", editTodoDTO.getId());
    editTodoDTO.setEditable(false);

    Todo todoUpdate = Todo.newBuilder()
        .withId(editTodoDTO.getId())
        .withName(editTodoDTO.getName())
        .withDescription(editTodoDTO.getDescription())
        .withDueDate(editTodoDTO.getDueDate())
        .build();

    this.todoRepository.update(todoUpdate);
  }

  private void loadTodos() {
    this.todos = this.getAllTodos();
  }

  private void loadTodo() {
    this.baseTodoDTO = new BaseTodoDTO();
  }

  public BaseTodoDTO getBaseTodoDTO() {
    return baseTodoDTO;
  }

  public void setBaseTodoDTO(final BaseTodoDTO baseTodoDTO) {
    this.baseTodoDTO = baseTodoDTO;
  }

  public void setTodos(final List<EditTodoDTO> todos) {
    this.todos = todos;
  }

  public List<EditTodoDTO> getTodos() {
    return todos;
  }
}
