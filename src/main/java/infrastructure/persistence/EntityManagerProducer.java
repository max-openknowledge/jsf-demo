package infrastructure.persistence;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 * CDI producer that provides the {@link EntityManager}.
 */
@Dependent
public class EntityManagerProducer {

  private static final String DEFAULT = "default";

  @javax.persistence.PersistenceUnit(unitName = DEFAULT)
  private EntityManagerFactory entityManagerFactory;

  @Produces
  @ApplicationScoped
  public EntityManager createEntityManager() {
    return entityManagerFactory.createEntityManager();
  }

  public void close(
      @Disposes
      final EntityManager entityManager) {
    entityManager.close();
  }
}
