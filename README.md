# JSF Todo demo

A example JSF App running inside a Wildfly container connected to a Postgres Database.

## Requirements

- Docker
- docker-compose
- Maven

## Run

To run the app you have to execute: 
```bash
docker-compose up --build
```

Navigate to `localhost:8080/jsf-todo/index.jsf`

## Test

To test the application run:
```bash
mvn clean verify
```


## Base Image

You can find the base image from the Dockerfile [here](https://gitlab.com/max-develop/wildfy-pq)
